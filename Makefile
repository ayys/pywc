setup:
	pipenv install

test: test-no-of-words test-file

test-no-of-words: tests/test_no_of_words.py
	pipenv run python -m pytest -v

test-file:
	./main.py test_file
