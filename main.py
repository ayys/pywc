#!/usr/bin/env python3

from src.words import get_words, count_words
from src.sentences import get_sentences

import argparse


parser = argparse.ArgumentParser(prog='pywc',
                                 description='pywc - A word counter with multi-lingual support',
                                 epilog="म्याउ!")
parser.add_argument('files', metavar='File', type=str, nargs=1,
                    help='Specify one or more files to use as inputs')



args = parser.parse_args()

files = args.files

def pretty_print_words(words):
    print("|{0:-^30}\t|{1:-^10}|".format("", ""))
    print("|{0:30}\t|{1:>10}|".format("Word", "Frequency"))
    print("|{0:-^30}\t|{1:-^10}|".format("", ""))
    for word in words:
        print("|{0:30}\t|{1:10}|".format(word[0], word[1]))

with open(files[0]) as f:
    lines = f.read()
    sentences = tuple(get_sentences(lines).elements())
    sentences_to_print = map(lambda _: "{}. {}".format(_[0], _[1]), enumerate(sentences)) # enumerated sentences
    words = sorted(tuple(get_words(lines).items()), key = lambda _: _[1])
    print("\n".join(sentences_to_print)) # print the sentences
    print("\n")
    pretty_print_words(words)   # print the words
