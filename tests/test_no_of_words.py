from src.words import count_words


'''Test Multiple Words'''
def test_multiple_words ():
    assert count_words("hello world") == 2 # done
'''Test Multiple Words With Four Per Em Space At End'''
def test_multiple_words_with_four_per_em_space_at_end ():
    assert count_words("hello world ") == 2 # done
'''Test Multiple Words With Four Per Em Space At Start'''
def test_multiple_words_with_four_per_em_space_at_start ():
    assert count_words(" hello world") == 2 # done
'''Test Multiple Words With Four Per Em Space At Start And End'''
def test_multiple_words_with_four_per_em_space_at_start_and_end ():
    assert count_words(" hello world ") == 2 # done
'''Test Multiple Words With Hair Space At End'''
def test_multiple_words_with_hair_space_at_end ():
    assert count_words("hello world ") == 2 # done
'''test Multiple Words With Hair Space At Start'''
def test_multiple_words_with_hair_space_at_start ():
    assert count_words(" hello world") == 2 # done
'''Test Multiple Words With Hair Space At Start And End'''
def test_multiple_words_with_hair_space_at_start_and_end ():
    assert count_words(" hello world ") == 2 # done
'''Test Multiple Words With Multiple Four Per Em Space At End'''
def test_multiple_words_with_multiple_four_per_em_space_at_end ():
    assert count_words("hello             world                      ") == 2 # done
'''Test Multiple Words With Multiple Four Per Em Space At Start'''
def test_multiple_words_with_multiple_four_per_em_space_at_start ():
    assert count_words("                              hello       world") == 2 # done
'''Test Multiple Words With Multiple Four Per Em Space At Start And End'''
def test_multiple_words_with_multiple_four_per_em_space_at_start_and_end ():
    assert count_words("                  hello                  world           ") == 2 # done
'''Test Multiple Words With Multiple Hair Space At End'''
def test_multiple_words_with_multiple_hair_space_at_end ():
    assert count_words("hello               world                  ") == 2 # done
'''Test Multiple Words With Multiple Hair Space At Start'''
def test_multiple_words_with_multiple_hair_space_at_start ():
    assert count_words("                  hello                         world") == 2 # done
'''Test Multiple Words With Multiple Hair Space At Start And End'''
def test_multiple_words_with_multiple_hair_space_at_start_and_end ():
    assert count_words("           hello             world                ") == 2 # done
'''Test Multiple Words With Multiple Punctuation Space At End'''
def test_multiple_words_with_multiple_punctuation_space_at_end ():
    assert count_words("hello world       ") == 2 # done
'''Test Multiple Words With Multiple Punctuation Space At Start'''
def test_multiple_words_with_multiple_punctuation_space_at_start ():
    assert count_words("             hello       world") == 2 # done
'''Test Multiple Words With Multiple Punctuation Space At Start And End'''
def test_multiple_words_with_multiple_punctuation_space_at_start_and_end ():
    assert count_words("            hello world          ") == 2 # done
'''Test Multiple Words With Multiple Six Per Em Space At End'''
def test_multiple_words_with_multiple_six_per_em_space_at_end ():
    assert count_words("hello        world              ") == 2 # done
'''Test Multiple Words With Multiple Six Per Em Space At Start'''
def test_multiple_words_with_multiple_six_per_em_space_at_start ():
    assert count_words("            hello    world") == 2 # done
'''Test Multiple Words With Multiple Six Per Em Space At Start And End'''
def test_multiple_words_with_multiple_six_per_em_space_at_start_and_end ():
    assert count_words("             hello          world         ") == 2 # done
'''Test Multiple Words With Multiple Space At Start And End'''
def test_multiple_words_with_multiple_space_at_start_and_end ():
    assert count_words("         hello world       ") == 2 # done
'''Test Multiple Words With Multiple Tab At End'''
def test_multiple_words_with_multiple_tab_at_end ():
    assert count_words("hello	world			") == 2 # done
'''Test Multiple Words With Multiple Tab At Start'''
def test_multiple_words_with_multiple_tab_at_start ():
    assert count_words("		hello	world") == 2 # done
'''test Multiple Words With Multiple Tab At Start And End'''
def test_multiple_words_with_multiple_tab_at_start_and_end ():
    assert count_words("			hello	world		") == 2 # done
'''Test Multiple Words With Multiple Thin Space At End'''
def test_multiple_words_with_multiple_thin_space_at_end ():
    assert count_words("hello world         ") == 2 # done
'''Test Multiple Words With Multiple Thin Space At Start'''
def test_multiple_words_with_multiple_thin_space_at_start ():
    assert count_words("           hello world") == 2 # done
'''Test Multiple Words With Multiple Thin Space At Start And End'''
def test_multiple_words_with_multiple_thin_space_at_start_and_end ():
    assert count_words("              hello    world         ") == 2 # done
'''Test Multiple Words With Multiple Zero Width Space At End'''
def test_multiple_words_with_multiple_zero_width_space_at_end ():
    assert count_words("hello​world​​​​​​​​​​​​​​​​​​​") == 2 # done
'''Test Multiple Words With Multiple Zero Width Space At Start'''
def test_multiple_words_with_multiple_zero_width_space_at_start ():
    assert count_words("​​​​​​​​​​​​​​​​​​​​​​​​​​​​​hello​world") == 2 # done
'''Test Multiple Words With Multiple Zero Width Space At Start And End'''
def test_multiple_words_with_multiple_zero_width_space_at_start_and_end ():
    assert count_words("​​​​​​​​​​hello​world​​​​​​​​​​​​​​​") == 2 # done
'''Test Multiple Words With Punctuation Space At End'''
def test_multiple_words_with_punctuation_space_at_end ():
    assert count_words("hello world ") == 2 # done
'''Test Multiple Words With Punctuation Space At Start'''
def test_multiple_words_with_punctuation_space_at_start ():
    assert count_words(" hello world") == 2 # done
'''Test Multiple Words With Punctuation Space At Start And End'''
def test_multiple_words_with_punctuation_space_at_start_and_end ():
    assert count_words(" hello world ") == 2 # done
'''Test Multiple Words With Six Per Em Space At End'''
def test_multiple_words_with_six_per_em_space_at_end ():
    assert count_words("hello world ") == 2 # done
'''Test Multiple Words With Six Per Em Space At Start'''
def test_multiple_words_with_six_per_em_space_at_start ():
    assert count_words(" hello world") == 2 # done
'''Test Multiple Words With Six Per Em Space At Start And End'''
def test_multiple_words_with_six_per_em_space_at_start_and_end ():
    assert count_words(" hello world ") == 2 # done
'''Test Multiple Words With Space At Start And End'''
def test_multiple_words_with_space_at_start_and_end ():
    assert count_words("    hello world   ") == 2 # done
'''Test Multiple Words With Tab At End'''
def test_multiple_words_with_tab_at_end ():
    assert count_words("hello world	") == 2 # done
'''Test Multiple Words With Tab At Start'''
def test_multiple_words_with_tab_at_start ():
    assert count_words("	hello world") == 2 # done
'''test Multiple Words With Tab At Start And End'''
def test_multiple_words_with_tab_at_start_and_end ():
    assert count_words("	hello world	") == 2 # done
'''Test Multiple Words With Thin Space At End'''
def test_multiple_words_with_thin_space_at_end ():
    assert count_words("hello world ") == 2 # done
'''Test Multiple Words With Thin Space At Start'''
def test_multiple_words_with_thin_space_at_start ():
    assert count_words(" hello world") == 2 # done
'''Test Multiple Words With Thin Space At Start And End'''
def test_multiple_words_with_thin_space_at_start_and_end ():
    assert count_words(" hello world ") == 2 # done
'''Test Multiple Words With Zero Width Space At End'''
def test_multiple_words_with_zero_width_space_at_end ():
    assert count_words("hello​world​") == 2 # done
'''Test Multiple Words With Zero Width Space At Start'''
def test_multiple_words_with_zero_width_space_at_start ():
    assert count_words("​hello​world") == 2 # done
'''Test Multiple Words With Zero Width Space At Start And End'''
def test_multiple_words_with_zero_width_space_at_start_and_end ():
    assert count_words("​hello​world​") == 2 # done
'''Test Mutliple Words With Multiple Space At End'''
def test_mutliple_words_with_multiple_space_at_end ():
    assert count_words("hello world      ") == 2 # done
'''Test Mutliple Words With Multiple Space At Start'''
def test_mutliple_words_with_multiple_space_at_start ():
    assert count_words("      hello world") == 2 # done
'''Test Mutliple Words With Space At End'''
def test_mutliple_words_with_space_at_end ():
    assert count_words("hello world ") == 2 # done
'''Test Mutliple Words With Space At Start'''
def test_mutliple_words_with_space_at_start ():
    assert count_words(" hello world") == 2 # done
'''Test Single Word'''
def test_single_word():
    assert count_words("hello") == 1 # done
'''Test Single Word With Braille Blank At End'''
def test_single_word_with_braille_blank_at_end ():
    assert count_words("hello⠀") == 1 # done
'''Test Single Word With Braille Blank At Start'''
def test_single_word_with_braille_blank_at_start ():
    assert count_words("⠀hello") == 1 # done
'''Test Single Word With Braille Blank At Start And End'''
def test_single_word_with_braille_blank_at_start_and_end ():
    assert count_words("⠀hello⠀") == 1 # done
'''Test Single Word With Em Space At End'''
def test_single_word_with_em_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Em Space At Start'''
def test_single_word_with_em_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Em Space At Start And End'''
def test_single_word_with_em_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With En Space At End'''
def test_single_word_with_en_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With En Space At Start'''
def test_single_word_with_en_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With En Space At Start And End'''
def test_single_word_with_en_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With Figure Space At End'''
def test_single_word_with_figure_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Figure Space At Start'''
def test_single_word_with_figure_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Figure Space At Start And End'''
def test_single_word_with_figure_space_at_start_and_end ():
    assert count_words(" world ") == 1 # done
'''Test Single Word With Four Per Em Space At End'''
def test_single_word_with_four_per_em_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Four Per Em Space At Start'''
def test_single_word_with_four_per_em_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Four Per Em Space At Start And End'''
def test_single_word_with_four_per_em_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With Hair Space At End'''
def test_single_word_with_hair_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Hair Space At Start'''
def test_single_word_with_hair_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Hair Space At Start And End'''
def test_single_word_with_hair_space_at_start_and_end ():
    assert count_words("                hello                   ") == 1 # done
'''Test Single Word With Multiple Braille Blank At End'''
def test_single_word_with_multiple_braille_blank_at_end ():
    assert count_words("hello⠀⠀⠀⠀⠀⠀⠀⠀") == 1 # done
'''Test Single Word With Multiple Braille Blank At Start'''
def test_single_word_with_multiple_braille_blank_at_start ():
    assert count_words("⠀⠀⠀⠀⠀hello") == 1 # done
'''Test Single Word With Multiple Braille Blank At Start And End'''
def test_single_word_with_multiple_braille_blank_at_start_and_end ():
    assert count_words("⠀⠀⠀⠀⠀⠀⠀hello⠀⠀⠀⠀⠀⠀⠀⠀⠀") == 1 # done
'''Test Single Word With Multiple Em Space At End'''
def test_single_word_with_multiple_em_space_at_end ():
    assert count_words("hello     ") == 1 # done
'''Test Single Word With Multiple Em Space At Start'''
def test_single_word_with_multiple_em_space_at_start ():
    assert count_words("      hello") == 1 # done
'''Test Single Word With Multiple Em Space At Start And End'''
def test_single_word_with_multiple_em_space_at_start_and_end ():
    assert count_words("    hello     ") == 1 # done
'''Test Single Word With Multiple En Space At End'''
def test_single_word_with_multiple_en_space_at_end ():
    assert count_words("hello    ") == 1 # done
'''Test Single Word With Multiple En Space At Start'''
def test_single_word_with_multiple_en_space_at_start ():
    assert count_words("    hello") == 1 # done
'''Test Single Word With Multiple En Space At Start And End'''
def test_single_word_with_multiple_en_space_at_start_and_end ():
    assert count_words("     hello     ") == 1 # done
'''Test Single Word With Multiple Figure Space At End'''
def test_single_word_with_multiple_figure_space_at_end ():
    assert count_words("hello         ") == 1 # done
'''Test Single Word With Multiple Figure Space At Start'''
def test_single_word_with_multiple_figure_space_at_start ():
    assert count_words("      hello") == 1 # done
def test_single_word_with_multiple_figure_space_at_start ():
    assert count_words("    hello           ") == 1 # done
'''Test Single Word With Multiple Figure Space At Start And End'''
def test_single_word_with_multiple_figure_space_at_start_and_end ():
    assert count_words("      hello      ") == 1 # done
'''Test Single Word With Multiple Four Per Em Space At End'''
def test_single_word_with_multiple_four_per_em_space_at_end ():
    assert count_words("hello           ") == 1 # done
'''Test Single Word With Multiple Four Per Em Space At Start'''
def test_single_word_with_multiple_four_per_em_space_at_start ():
    assert count_words("           hello") == 1 # done
'''Test Single Word With Multiple Four Per Em Space At Start And End'''
def test_single_word_with_multiple_four_per_em_space_at_start_and_end ():
    assert count_words("               hello           ") == 1 # done
'''Test Single Word With Multiple Hair Space At End'''
def test_single_word_with_multiple_hair_space_at_end ():
    assert count_words("hello        ") == 1 # done
'''Test Single Word With Multiple Hair Space At Start'''
def test_single_word_with_multiple_hair_space_at_start ():
    assert count_words("              hello") == 1 # done
'''Test Single Word With Multiple Hair Space At Start And End'''
def test_single_word_with_multiple_hair_space_at_start_and_end ():
    assert count_words("                hello           ") == 1 # done
'''Test Single Word With Multiple Punctuation Space At End'''
def test_single_word_with_multiple_punctuation_space_at_end ():
    assert count_words("hello          ") == 1 # done
'''Test Single Word With Multiple Punctuation Space At Start'''
def test_single_word_with_multiple_punctuation_space_at_start ():
    assert count_words("          hello") == 1 # done
'''Test Single Word With Multiple Punctuation Space At Start And End'''
def test_single_word_with_multiple_punctuation_space_at_start_and_end ():
    assert count_words("                 hello     ") == 1 # done
'''Test Single Word With Multiple Six Per Em Space At End'''
def test_single_word_with_multiple_six_per_em_space_at_end ():
    assert count_words("hello              ") == 1 # done
'''Test Single Word With Multiple Six Per Em Space At Start'''
def test_single_word_with_multiple_six_per_em_space_at_start ():
    assert count_words("                 hello") == 1 # done
'''Test Single Word With Multiple Six Per Em Space At Start And End'''
def test_single_word_with_multiple_six_per_em_space_at_start_and_end ():
    assert count_words("               hello               ") == 1 # done
'''Test Single Word With Multiple Space At End'''
def test_single_word_with_multiple_space_at_end():
    assert count_words("hello           ") == 1 # done
'''Test Single Word With Multiple Space At Start'''
def test_single_word_with_multiple_space_at_start():
    assert count_words("              hello") == 1 # done
'''Test Single Word With Multiple Space At Start And End'''
def test_single_word_with_multiple_space_at_start_and_end():
    assert count_words("             hello         ") == 1 # done
'''Test Single Word With Multiple Tab At End'''
def test_single_word_with_multiple_tab_at_end ():
    assert count_words("hello			") == 1 # done
'''Test Single Word With Multiple Tab At Start'''
def test_single_word_with_multiple_tab_at_start ():
    assert count_words("		hello") == 1 # done
'''Test Single Word With Multiple Tab At Start And End'''
def test_single_word_with_multiple_tab_at_start_and_end ():
    assert count_words("		hello			") == 1 # done
'''Test Single Word With Multiple Thin Space At End'''
def test_single_word_with_multiple_thin_space_at_end ():
    assert count_words("hello      ") == 1 # done
'''Test Single Word With Multiple Thin Space At Start'''
def test_single_word_with_multiple_thin_space_at_start ():
    assert count_words("      hello") == 1 # done
'''Test Single Word With Multiple Thin Space At Start And End'''
def test_single_word_with_multiple_thin_space_at_start_and_end ():
    assert count_words("    hello            ") == 1 # done
'''Test Single Word With Multiple Three Per Em Space At End'''
def test_single_word_with_multiple_three_per_em_space_at_end ():
    assert count_words("hello                ") == 1 # done
'''Test Single Word With Multiple Three Per Em Space At Start'''
def test_single_word_with_multiple_three_per_em_space_at_start ():
    assert count_words("          hello") == 1 # done
'''Test Single Word With Multiple Three Per Em Space At Start And End'''
def test_single_word_with_multiple_three_per_em_space_at_start_and_end ():
    assert count_words("     hello            ") == 1 # done
'''Test Single Word With Multiple Zero Width Space At End'''
def test_single_word_with_multiple_zero_width_space_at_end ():
    assert count_words("hello​​​​​") == 1 # done
'''Test Single Word With Multiple Zero Width Space At Start'''
def test_single_word_with_multiple_zero_width_space_at_start ():
    assert count_words("​​​hello") == 1 # done
'''Test Single Word With Multiple Zero Width Space At Start And End'''
def test_single_word_with_multiple_zero_width_space_at_start_and_end ():
    assert count_words("​​​​​​​​​​​​​​​​​​​​​​​​hello​​​​​​​​​​​​​​") == 1 # done
'''Test Single Word With Punctuation Space At End'''
def test_single_word_with_punctuation_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Punctuation Space At Start'''
def test_single_word_with_punctuation_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Punctuation Space At Start And End'''
def test_single_word_with_punctuation_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''test Single Word With Six Per Em Space At End'''
def test_single_word_with_six_per_em_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Six Per Em Space At Start'''
def test_single_word_with_six_per_em_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Six Per Em Space At Start And End'''
def test_single_word_with_six_per_em_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With Space At End'''
def test_single_word_with_space_at_end():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Space At Start'''
def test_single_word_with_space_at_start():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Space At Start And End'''
def test_single_word_with_space_at_start_and_end():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With Tab At End'''
def test_single_word_with_tab_at_end ():
    assert count_words("hello	") == 1 # done
'''Test Single Word With Tab At Start'''
def test_single_word_with_tab_at_start ():
    assert count_words("	world") == 1 # done
'''Test Single Word With Tab At Start And End'''
def test_single_word_with_tab_at_start_and_end ():
    assert count_words("	world	") == 1 # done
'''Test Single Word With Thin Space At End'''
def test_single_word_with_thin_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Thin Space At Start'''
def test_single_word_with_thin_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Thin Space At Start And End'''
def test_single_word_with_thin_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With Three Per Em Space At End'''
def test_single_word_with_three_per_em_space_at_end ():
    assert count_words("hello ") == 1 # done
'''Test Single Word With Three Per Em Space At Start'''
def test_single_word_with_three_per_em_space_at_start ():
    assert count_words(" hello") == 1 # done
'''Test Single Word With Three Per Em Space At Start And End'''
def test_single_word_with_three_per_em_space_at_start_and_end ():
    assert count_words(" hello ") == 1 # done
'''Test Single Word With Zero Width Space At End'''
def test_single_word_with_zero_width_space_at_end ():
    assert count_words("world​") == 1 # done
'''Test Single Word With Zero Width Space At Start'''
def test_single_word_with_zero_width_space_at_start ():
    assert count_words("​world") == 1 # done
'''Test Single Word With Zero Width Space At Start And End'''
def test_single_word_with_zero_width_space_at_start_and_end ():
    assert count_words("​world​") == 1 # done

# Test With Non-ascii words

'''Test Multiple Words'''
def test_unicode_multiple_words ():
    assert count_words("नमस्ते संसार") == 2 # done
'''Test Multiple Words With Four Per Em Space At End'''
def test_unicode_multiple_words_with_four_per_em_space_at_end ():
    assert count_words("नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Four Per Em Space At Start'''
def test_unicode_multiple_words_with_four_per_em_space_at_start ():
    assert count_words(" नमस्ते संसार") == 2 # done
'''Test Multiple Words With Four Per Em Space At Start And End'''
def test_unicode_multiple_words_with_four_per_em_space_at_start_and_end ():
    assert count_words(" नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Hair Space At End'''
def test_unicode_multiple_words_with_hair_space_at_end ():
    assert count_words("नमस्ते संसार ") == 2 # done
'''test Multiple Words With Hair Space At Start'''
def test_unicode_multiple_words_with_hair_space_at_start ():
    assert count_words(" नमस्ते संसार") == 2 # done
'''Test Multiple Words With Hair Space At Start And End'''
def test_unicode_multiple_words_with_hair_space_at_start_and_end ():
    assert count_words(" नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Multiple Four Per Em Space At End'''
def test_unicode_multiple_words_with_multiple_four_per_em_space_at_end ():
    assert count_words("नमस्ते             संसार                      ") == 2 # done
'''Test Multiple Words With Multiple Four Per Em Space At Start'''
def test_unicode_multiple_words_with_multiple_four_per_em_space_at_start ():
    assert count_words("                              नमस्ते       संसार") == 2 # done
'''Test Multiple Words With Multiple Four Per Em Space At Start And End'''
def test_unicode_multiple_words_with_multiple_four_per_em_space_at_start_and_end ():
    assert count_words("                  नमस्ते                  संसार           ") == 2 # done
'''Test Multiple Words With Multiple Hair Space At End'''
def test_unicode_multiple_words_with_multiple_hair_space_at_end ():
    assert count_words("नमस्ते               संसार                  ") == 2 # done
'''Test Multiple Words With Multiple Hair Space At Start'''
def test_unicode_multiple_words_with_multiple_hair_space_at_start ():
    assert count_words("                  नमस्ते                         संसार") == 2 # done
'''Test Multiple Words With Multiple Hair Space At Start And End'''
def test_unicode_multiple_words_with_multiple_hair_space_at_start_and_end ():
    assert count_words("           नमस्ते             संसार                ") == 2 # done
'''Test Multiple Words With Multiple Punctuation Space At End'''
def test_unicode_multiple_words_with_multiple_punctuation_space_at_end ():
    assert count_words("नमस्ते संसार       ") == 2 # done
'''Test Multiple Words With Multiple Punctuation Space At Start'''
def test_unicode_multiple_words_with_multiple_punctuation_space_at_start ():
    assert count_words("             नमस्ते       संसार") == 2 # done
'''Test Multiple Words With Multiple Punctuation Space At Start And End'''
def test_unicode_multiple_words_with_multiple_punctuation_space_at_start_and_end ():
    assert count_words("            नमस्ते संसार          ") == 2 # done
'''Test Multiple Words With Multiple Six Per Em Space At End'''
def test_unicode_multiple_words_with_multiple_six_per_em_space_at_end ():
    assert count_words("नमस्ते        संसार              ") == 2 # done
'''Test Multiple Words With Multiple Six Per Em Space At Start'''
def test_unicode_multiple_words_with_multiple_six_per_em_space_at_start ():
    assert count_words("            नमस्ते    संसार") == 2 # done
'''Test Multiple Words With Multiple Six Per Em Space At Start And End'''
def test_unicode_multiple_words_with_multiple_six_per_em_space_at_start_and_end ():
    assert count_words("             नमस्ते          संसार         ") == 2 # done
'''Test Multiple Words With Multiple Space At Start And End'''
def test_unicode_multiple_words_with_multiple_space_at_start_and_end ():
    assert count_words("         नमस्ते संसार       ") == 2 # done
'''Test Multiple Words With Multiple Tab At End'''
def test_unicode_multiple_words_with_multiple_tab_at_end ():
    assert count_words("नमस्ते	संसार			") == 2 # done
'''Test Multiple Words With Multiple Tab At Start'''
def test_unicode_multiple_words_with_multiple_tab_at_start ():
    assert count_words("		नमस्ते	संसार") == 2 # done
'''test Multiple Words With Multiple Tab At Start And End'''
def test_unicode_multiple_words_with_multiple_tab_at_start_and_end ():
    assert count_words("			नमस्ते	संसार		") == 2 # done
'''Test Multiple Words With Multiple Thin Space At End'''
def test_unicode_multiple_words_with_multiple_thin_space_at_end ():
    assert count_words("नमस्ते संसार         ") == 2 # done
'''Test Multiple Words With Multiple Thin Space At Start'''
def test_unicode_multiple_words_with_multiple_thin_space_at_start ():
    assert count_words("           नमस्ते संसार") == 2 # done
'''Test Multiple Words With Multiple Thin Space At Start And End'''
def test_unicode_multiple_words_with_multiple_thin_space_at_start_and_end ():
    assert count_words("              नमस्ते    संसार         ") == 2 # done
'''Test Multiple Words With Multiple Zero Width Space At End'''
def test_unicode_multiple_words_with_multiple_zero_width_space_at_end ():
    assert count_words("नमस्ते​संसार​​​​​​​​​​​​​​​​​​​") == 2 # done
'''Test Multiple Words With Multiple Zero Width Space At Start'''
def test_unicode_multiple_words_with_multiple_zero_width_space_at_start ():
    assert count_words("​​​​​​​​​​​​​​​​​​​​​​​​​​​​​नमस्ते​संसार") == 2 # done
'''Test Multiple Words With Multiple Zero Width Space At Start And End'''
def test_unicode_multiple_words_with_multiple_zero_width_space_at_start_and_end ():
    assert count_words("​​​​​​​​​​नमस्ते​संसार​​​​​​​​​​​​​​​") == 2 # done
'''Test Multiple Words With Punctuation Space At End'''
def test_unicode_multiple_words_with_punctuation_space_at_end ():
    assert count_words("नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Punctuation Space At Start'''
def test_unicode_multiple_words_with_punctuation_space_at_start ():
    assert count_words(" नमस्ते संसार") == 2 # done
'''Test Multiple Words With Punctuation Space At Start And End'''
def test_unicode_multiple_words_with_punctuation_space_at_start_and_end ():
    assert count_words(" नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Six Per Em Space At End'''
def test_unicode_multiple_words_with_six_per_em_space_at_end ():
    assert count_words("नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Six Per Em Space At Start'''
def test_unicode_multiple_words_with_six_per_em_space_at_start ():
    assert count_words(" नमस्ते संसार") == 2 # done
'''Test Multiple Words With Six Per Em Space At Start And End'''
def test_unicode_multiple_words_with_six_per_em_space_at_start_and_end ():
    assert count_words(" नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Space At Start And End'''
def test_unicode_multiple_words_with_space_at_start_and_end ():
    assert count_words("    नमस्ते संसार   ") == 2 # done
'''Test Multiple Words With Tab At End'''
def test_unicode_multiple_words_with_tab_at_end ():
    assert count_words("नमस्ते संसार	") == 2 # done
'''Test Multiple Words With Tab At Start'''
def test_unicode_multiple_words_with_tab_at_start ():
    assert count_words("	नमस्ते संसार") == 2 # done
'''test Multiple Words With Tab At Start And End'''
def test_unicode_multiple_words_with_tab_at_start_and_end ():
    assert count_words("	नमस्ते संसार	") == 2 # done
'''Test Multiple Words With Thin Space At End'''
def test_unicode_multiple_words_with_thin_space_at_end ():
    assert count_words("नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Thin Space At Start'''
def test_unicode_multiple_words_with_thin_space_at_start ():
    assert count_words(" नमस्ते संसार") == 2 # done
'''Test Multiple Words With Thin Space At Start And End'''
def test_unicode_multiple_words_with_thin_space_at_start_and_end ():
    assert count_words(" नमस्ते संसार ") == 2 # done
'''Test Multiple Words With Zero Width Space At End'''
def test_unicode_multiple_words_with_zero_width_space_at_end ():
    assert count_words("नमस्ते​संसार​") == 2 # done
'''Test Multiple Words With Zero Width Space At Start'''
def test_unicode_multiple_words_with_zero_width_space_at_start ():
    assert count_words("​नमस्ते​संसार") == 2 # done
'''Test Multiple Words With Zero Width Space At Start And End'''
def test_unicode_multiple_words_with_zero_width_space_at_start_and_end ():
    assert count_words("​नमस्ते​संसार​") == 2 # done
'''Test Mutliple Words With Multiple Space At End'''
def test_unicode_mutliple_words_with_multiple_space_at_end ():
    assert count_words("नमस्ते संसार      ") == 2 # done
'''Test Mutliple Words With Multiple Space At Start'''
def test_unicode_mutliple_words_with_multiple_space_at_start ():
    assert count_words("      नमस्ते संसार") == 2 # done
'''Test Mutliple Words With Space At End'''
def test_unicode_mutliple_words_with_space_at_end ():
    assert count_words("नमस्ते संसार ") == 2 # done
'''Test Mutliple Words With Space At Start'''
def test_unicode_mutliple_words_with_space_at_start ():
    assert count_words(" नमस्ते संसार") == 2 # done
'''Test Single Word'''
def test_unicode_single_word():
    assert count_words("नमस्ते") == 1 # done
'''Test Single Word With Braille Blank At End'''
def test_unicode_single_word_with_braille_blank_at_end ():
    assert count_words("नमस्ते⠀") == 1 # done
'''Test Single Word With Braille Blank At Start'''
def test_unicode_single_word_with_braille_blank_at_start ():
    assert count_words("⠀नमस्ते") == 1 # done
'''Test Single Word With Braille Blank At Start And End'''
def test_unicode_single_word_with_braille_blank_at_start_and_end ():
    assert count_words("⠀नमस्ते⠀") == 1 # done
'''Test Single Word With Em Space At End'''
def test_unicode_single_word_with_em_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Em Space At Start'''
def test_unicode_single_word_with_em_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Em Space At Start And End'''
def test_unicode_single_word_with_em_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With En Space At End'''
def test_unicode_single_word_with_en_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With En Space At Start'''
def test_unicode_single_word_with_en_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With En Space At Start And End'''
def test_unicode_single_word_with_en_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With Figure Space At End'''
def test_unicode_single_word_with_figure_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Figure Space At Start'''
def test_unicode_single_word_with_figure_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Figure Space At Start And End'''
def test_unicode_single_word_with_figure_space_at_start_and_end ():
    assert count_words(" संसार ") == 1 # done
'''Test Single Word With Four Per Em Space At End'''
def test_unicode_single_word_with_four_per_em_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Four Per Em Space At Start'''
def test_unicode_single_word_with_four_per_em_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Four Per Em Space At Start And End'''
def test_unicode_single_word_with_four_per_em_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With Hair Space At End'''
def test_unicode_single_word_with_hair_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Hair Space At Start'''
def test_unicode_single_word_with_hair_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Hair Space At Start And End'''
def test_unicode_single_word_with_hair_space_at_start_and_end ():
    assert count_words("                नमस्ते                   ") == 1 # done
'''Test Single Word With Multiple Braille Blank At End'''
def test_unicode_single_word_with_multiple_braille_blank_at_end ():
    assert count_words("नमस्ते⠀⠀⠀⠀⠀⠀⠀⠀") == 1 # done
'''Test Single Word With Multiple Braille Blank At Start'''
def test_unicode_single_word_with_multiple_braille_blank_at_start ():
    assert count_words("⠀⠀⠀⠀⠀नमस्ते") == 1 # done
'''Test Single Word With Multiple Braille Blank At Start And End'''
def test_unicode_single_word_with_multiple_braille_blank_at_start_and_end ():
    assert count_words("⠀⠀⠀⠀⠀⠀⠀नमस्ते⠀⠀⠀⠀⠀⠀⠀⠀⠀") == 1 # done
'''Test Single Word With Multiple Em Space At End'''
def test_unicode_single_word_with_multiple_em_space_at_end ():
    assert count_words("नमस्ते     ") == 1 # done
'''Test Single Word With Multiple Em Space At Start'''
def test_unicode_single_word_with_multiple_em_space_at_start ():
    assert count_words("      नमस्ते") == 1 # done
'''Test Single Word With Multiple Em Space At Start And End'''
def test_unicode_single_word_with_multiple_em_space_at_start_and_end ():
    assert count_words("    नमस्ते     ") == 1 # done
'''Test Single Word With Multiple En Space At End'''
def test_unicode_single_word_with_multiple_en_space_at_end ():
    assert count_words("नमस्ते    ") == 1 # done
'''Test Single Word With Multiple En Space At Start'''
def test_unicode_single_word_with_multiple_en_space_at_start ():
    assert count_words("    नमस्ते") == 1 # done
'''Test Single Word With Multiple En Space At Start And End'''
def test_unicode_single_word_with_multiple_en_space_at_start_and_end ():
    assert count_words("     नमस्ते     ") == 1 # done
'''Test Single Word With Multiple Figure Space At End'''
def test_unicode_single_word_with_multiple_figure_space_at_end ():
    assert count_words("नमस्ते         ") == 1 # done
'''Test Single Word With Multiple Figure Space At Start'''
def test_unicode_single_word_with_multiple_figure_space_at_start ():
    assert count_words("      नमस्ते") == 1 # done
def test_unicode_single_word_with_multiple_figure_space_at_start ():
    assert count_words("    नमस्ते           ") == 1 # done
'''Test Single Word With Multiple Figure Space At Start And End'''
def test_unicode_single_word_with_multiple_figure_space_at_start_and_end ():
    assert count_words("      नमस्ते      ") == 1 # done
'''Test Single Word With Multiple Four Per Em Space At End'''
def test_unicode_single_word_with_multiple_four_per_em_space_at_end ():
    assert count_words("नमस्ते           ") == 1 # done
'''Test Single Word With Multiple Four Per Em Space At Start'''
def test_unicode_single_word_with_multiple_four_per_em_space_at_start ():
    assert count_words("           नमस्ते") == 1 # done
'''Test Single Word With Multiple Four Per Em Space At Start And End'''
def test_unicode_single_word_with_multiple_four_per_em_space_at_start_and_end ():
    assert count_words("               नमस्ते           ") == 1 # done
'''Test Single Word With Multiple Hair Space At End'''
def test_unicode_single_word_with_multiple_hair_space_at_end ():
    assert count_words("नमस्ते        ") == 1 # done
'''Test Single Word With Multiple Hair Space At Start'''
def test_unicode_single_word_with_multiple_hair_space_at_start ():
    assert count_words("              नमस्ते") == 1 # done
'''Test Single Word With Multiple Hair Space At Start And End'''
def test_unicode_single_word_with_multiple_hair_space_at_start_and_end ():
    assert count_words("                नमस्ते           ") == 1 # done
'''Test Single Word With Multiple Punctuation Space At End'''
def test_unicode_single_word_with_multiple_punctuation_space_at_end ():
    assert count_words("नमस्ते          ") == 1 # done
'''Test Single Word With Multiple Punctuation Space At Start'''
def test_unicode_single_word_with_multiple_punctuation_space_at_start ():
    assert count_words("          नमस्ते") == 1 # done
'''Test Single Word With Multiple Punctuation Space At Start And End'''
def test_unicode_single_word_with_multiple_punctuation_space_at_start_and_end ():
    assert count_words("                 नमस्ते     ") == 1 # done
'''Test Single Word With Multiple Six Per Em Space At End'''
def test_unicode_single_word_with_multiple_six_per_em_space_at_end ():
    assert count_words("नमस्ते              ") == 1 # done
'''Test Single Word With Multiple Six Per Em Space At Start'''
def test_unicode_single_word_with_multiple_six_per_em_space_at_start ():
    assert count_words("                 नमस्ते") == 1 # done
'''Test Single Word With Multiple Six Per Em Space At Start And End'''
def test_unicode_single_word_with_multiple_six_per_em_space_at_start_and_end ():
    assert count_words("               नमस्ते               ") == 1 # done
'''Test Single Word With Multiple Space At End'''
def test_unicode_single_word_with_multiple_space_at_end():
    assert count_words("नमस्ते           ") == 1 # done
'''Test Single Word With Multiple Space At Start'''
def test_unicode_single_word_with_multiple_space_at_start():
    assert count_words("              नमस्ते") == 1 # done
'''Test Single Word With Multiple Space At Start And End'''
def test_unicode_single_word_with_multiple_space_at_start_and_end():
    assert count_words("             नमस्ते         ") == 1 # done
'''Test Single Word With Multiple Tab At End'''
def test_unicode_single_word_with_multiple_tab_at_end ():
    assert count_words("नमस्ते			") == 1 # done
'''Test Single Word With Multiple Tab At Start'''
def test_unicode_single_word_with_multiple_tab_at_start ():
    assert count_words("		नमस्ते") == 1 # done
'''Test Single Word With Multiple Tab At Start And End'''
def test_unicode_single_word_with_multiple_tab_at_start_and_end ():
    assert count_words("		नमस्ते			") == 1 # done
'''Test Single Word With Multiple Thin Space At End'''
def test_unicode_single_word_with_multiple_thin_space_at_end ():
    assert count_words("नमस्ते      ") == 1 # done
'''Test Single Word With Multiple Thin Space At Start'''
def test_unicode_single_word_with_multiple_thin_space_at_start ():
    assert count_words("      नमस्ते") == 1 # done
'''Test Single Word With Multiple Thin Space At Start And End'''
def test_unicode_single_word_with_multiple_thin_space_at_start_and_end ():
    assert count_words("    नमस्ते            ") == 1 # done
'''Test Single Word With Multiple Three Per Em Space At End'''
def test_unicode_single_word_with_multiple_three_per_em_space_at_end ():
    assert count_words("नमस्ते                ") == 1 # done
'''Test Single Word With Multiple Three Per Em Space At Start'''
def test_unicode_single_word_with_multiple_three_per_em_space_at_start ():
    assert count_words("          नमस्ते") == 1 # done
'''Test Single Word With Multiple Three Per Em Space At Start And End'''
def test_unicode_single_word_with_multiple_three_per_em_space_at_start_and_end ():
    assert count_words("     नमस्ते            ") == 1 # done
'''Test Single Word With Multiple Zero Width Space At End'''
def test_unicode_single_word_with_multiple_zero_width_space_at_end ():
    assert count_words("नमस्ते​​​​​") == 1 # done
'''Test Single Word With Multiple Zero Width Space At Start'''
def test_unicode_single_word_with_multiple_zero_width_space_at_start ():
    assert count_words("​​​नमस्ते") == 1 # done
'''Test Single Word With Multiple Zero Width Space At Start And End'''
def test_unicode_single_word_with_multiple_zero_width_space_at_start_and_end ():
    assert count_words("​​​​​​​​​​​​​​​​​​​​​​​​नमस्ते​​​​​​​​​​​​​​") == 1 # done
'''Test Single Word With Punctuation Space At End'''
def test_unicode_single_word_with_punctuation_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Punctuation Space At Start'''
def test_unicode_single_word_with_punctuation_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Punctuation Space At Start And End'''
def test_unicode_single_word_with_punctuation_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''test Single Word With Six Per Em Space At End'''
def test_unicode_single_word_with_six_per_em_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Six Per Em Space At Start'''
def test_unicode_single_word_with_six_per_em_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Six Per Em Space At Start And End'''
def test_unicode_single_word_with_six_per_em_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With Space At End'''
def test_unicode_single_word_with_space_at_end():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Space At Start'''
def test_unicode_single_word_with_space_at_start():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Space At Start And End'''
def test_unicode_single_word_with_space_at_start_and_end():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With Tab At End'''
def test_unicode_single_word_with_tab_at_end ():
    assert count_words("नमस्ते	") == 1 # done
'''Test Single Word With Tab At Start'''
def test_unicode_single_word_with_tab_at_start ():
    assert count_words("	संसार") == 1 # done
'''Test Single Word With Tab At Start And End'''
def test_unicode_single_word_with_tab_at_start_and_end ():
    assert count_words("	संसार	") == 1 # done
'''Test Single Word With Thin Space At End'''
def test_unicode_single_word_with_thin_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Thin Space At Start'''
def test_unicode_single_word_with_thin_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Thin Space At Start And End'''
def test_unicode_single_word_with_thin_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With Three Per Em Space At End'''
def test_unicode_single_word_with_three_per_em_space_at_end ():
    assert count_words("नमस्ते ") == 1 # done
'''Test Single Word With Three Per Em Space At Start'''
def test_unicode_single_word_with_three_per_em_space_at_start ():
    assert count_words(" नमस्ते") == 1 # done
'''Test Single Word With Three Per Em Space At Start And End'''
def test_unicode_single_word_with_three_per_em_space_at_start_and_end ():
    assert count_words(" नमस्ते ") == 1 # done
'''Test Single Word With Zero Width Space At End'''
def test_unicode_single_word_with_zero_width_space_at_end ():
    assert count_words("संसार​") == 1 # done
'''Test Single Word With Zero Width Space At Start'''
def test_unicode_single_word_with_zero_width_space_at_start ():
    assert count_words("​संसार") == 1 # done
'''Test Single Word With Zero Width Space At Start And End'''
def test_unicode_single_word_with_zero_width_space_at_start_and_end ():
    assert count_words("​संसार​") == 1 # done
