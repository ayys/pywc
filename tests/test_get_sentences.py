from src.sentences import get_sentences

# Test Get Latin Sentences
def test_get_latin_sentences ():
    assert tuple(get_sentences("Hello World. I am back. What a fun World.").elements()) == ("Hello World.", "I am back.", "What a fun World.",)
# Test Get Devanagari Sentences
def test_get_devanagari_sentences ():
    assert tuple(get_sentences("नमस्ते। मेरो नाम के हो।").elements()) == ("नमस्ते।", "मेरो नाम के हो।",)
# Test Get Sinhala Sentences
def test_get_sinhala_sentences ():
    assert tuple(get_sentences("හෙලෝ. මගේ නම අහඹුයි෴ ඔයාගේ නම කුමක් ද?").elements()) == ("හෙලෝ.", "මගේ නම අහඹුයි෴", "ඔයාගේ නම කුමක් ද?",)
# Test Get Urdu Sentences
def test_get_urdu_sentences ():
    assert tuple(get_sentences("ہیلو. میرا نام رینڈم ہے۔ آپ کا نام کیا ہے؟").elements()) == ("ہیلو.", "میرا نام رینڈم ہے۔","آپ کا نام کیا ہے؟")
# Test Get Ethiopic Sentences
def test_get_ethiopic_sentences ():
    assert tuple(get_sentences("ሰላም ልዑል. ተመለስኩ። እንዴት አዝናኝ ዓለም።").elements()) == ("ሰላም ልዑል.", "ተመለስኩ።", "እንዴት አዝናኝ ዓለም።")
# Test Get Sentences From String Without Any Stops
def test_get_sentences_from_string_without_any_stops ():
    assert tuple(get_sentences("hello world how are you").elements()) == ("hello world how are you",)
# Test Get Sentences From String With More Than One Full Stops
def test_get_sentences_from_string_with_more_than_one_consecutive_full_stops ():
    assert tuple(get_sentences("hello....how are you?").elements()) == ("hello....", "how are you?",)
# Test Get Sentences From String Wiht More Than One Question Marks
def test_get_sentences_from_string_wiht_more_than_one_question_marks ():
    assert tuple(get_sentences("huh??????What?").elements()) == ("huh??????", "What?")
# Test Get Sentences From String With Multiple Scripts
def test_get_sentences_from_string_with_multiple_scripts ():
    assert tuple(get_sentences("හෙලෝ. මගේ නම අහඹුයි෴ नमस्ते। मेरो नाम के हो।").elements()) == ("හෙලෝ.", "මගේ නම අහඹුයි෴", "नमस्ते।","मेरो नाम के हो।",)
# Test Sentences Ending In Exclamation Mark
def test_latin_sentences_ending_in_exclamation_mark ():
    assert tuple(get_sentences("Hello World! I am back! What a fun World!").elements()) == ("Hello World!", "I am back!", "What a fun World!",)
# Test Sentences Ending In Question Mark
def test_latin_sentences_ending_in_question_mark ():
    assert tuple(get_sentences("Hello World? I am back? What a fun World?").elements()) == ("Hello World?", "I am back?", "What a fun World?",)
# Test Devanagari Sentence Ending In Exclamation Mark
def test_devanagari_sentence_ending_in_exclamation_mark ():
    assert tuple(get_sentences("ओहो! कती राम्रो!").elements()) == ("ओहो!", "कती राम्रो!",)
# Test Sinhala Sentence Ending In Exclamation Mark
def test_sinhala_sentence_ending_in_exclamation_mark ():
    assert tuple(get_sentences("වාව්! ගොඩක් ලස්සනයි!").elements()) == ("වාව්!", "ගොඩක් ලස්සනයි!",)
# Test Urdu Sentence Ending In Exclamation Mark
def test_urdu_sentence_ending_in_exclamation_mark ():
    assert tuple(get_sentences("زبردست! بہت خوبصورت").elements()) == ("زبردست!", "بہت خوبصورت",)
# Test Ethiopic Sentence Ending In Exclamation Mark
def test_ethiopic_sentence_ending_in_exclamation_mark ():
    assert tuple(get_sentences("ዋዉ! በጣም ቆንጆ!").elements()) == ("ዋዉ!", "በጣም ቆንጆ!",)
# Test Devanagari Sentence Ending In Question Mark
def test_devanagari_sentence_ending_in_question_mark ():
    assert tuple(get_sentences("मेरो नाम? किन?").elements()) == ("मेरो नाम?", "किन?",)
# Test Sinhala Sentence Ending In Question Mark
def test_sinhala_sentence_ending_in_question_mark ():
    assert tuple(get_sentences("මගේ නම? මන්ද?").elements()) == ("මගේ නම?", "මන්ද?",)
# Test Urdu Sentence Ending In Question Mark
def test_urdu_sentence_ending_in_question_mark ():
    assert tuple(get_sentences("میرا نام؟ کیوں؟").elements()) == ("میرا نام؟", "کیوں؟",)
# Test Ethiopic Sentence Ending In Question Mark
def test_ethiopic_sentence_ending_in_question_mark ():
    assert tuple(get_sentences("ስሜ? ለምን?").elements()) == ("ስሜ?", "ለምን?",)
